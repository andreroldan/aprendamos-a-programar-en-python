# Cada nuevo término en la secuencia de Fibonacci es generado sumando los dos
# términos previos. Empezando con 1 y 2, los 10 primeros términos son:
# 1,2,3,5,8,13,21,34,55,89
# Encuentra la suma de todos los términos pares que no excedan los 4 millones.

# Autores: jaimebedoyae, Krizarmax, Ln0xx, jaggedmule14, Vitoya, standfox_

def fibonacci(numero):
    x = 1
    y = 0
    suma = y

    while y < numero:
        if y % 2 == 0:
            print(y)
            suma += y
        temporal = x
        x = y
        y = temporal + y

    return suma

print(fibonacci(4000000)) # 4613732