# Si hacemos una lista de todos los números naturales menores a 10
# que son multiplos de 3 o de 5, tenemos 3,5,6,9. La suma de estos
# multiplos es 23. Encuentra la suma de todos los multiplos de 3 o
# 5 menores a 1000.

# Autores: standfox_, Ln0xx, pepegibbons, jaimebedoyae, Vitoya

def multiplos3o5(numero):
    suma = 0
    for index in range(1, numero):
        if index % 3 == 0 or index % 5 == 0:
            print(index)
            suma += index # 0+3=3, 3+5=8, 8+6=14, 14+9=23
    
    return suma

print(multiplos3o5(1000)) # 233168
