chilaquiles = ['Tortillas', 'Salsa', 'Queso', 'Crema', 'Crema', 'Cebolla', 'Aceite']
# numeros = [5, 6, 3, 27, 1, 149]

print(chilaquiles.index('Crema'))
print(chilaquiles.count('Salsa'))
print(chilaquiles.count('Crema'))

chilaquiles.append('Cilantro')
chilaquiles.remove('Crema')

chilaquiles.sort()
print(chilaquiles)

chilaquiles_sin_cebolla = chilaquiles.copy()
chilaquiles_sin_cebolla.remove('Cebolla')
print(chilaquiles_sin_cebolla)

chilaquiles_pollo = chilaquiles.copy()
chilaquiles_pollo.append('Pollo')
chilaquiles_pollo.sort()
print(chilaquiles_pollo)


# chilaquiles.reverse()
# print(chilaquiles)


# numeros.sort()
# print(numeros)