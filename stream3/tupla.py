numeros = (4, 8, 'standfox_')
numeros[1] = 12  # ESTO DAA ERROR
print(numeros)
print(numeros[1])

tupla = (1, 27.4, 'standfox_', True, 'MeeMoont')
print(tupla)

lista_numeros = [1, 2, 3]
lista_numeros.extend(numeros)
print(lista_numeros)
print(f'El primer elemento de la lista es: {lista_numeros[0]} y el último es: {lista_numeros[-1]}')
print(lista_numeros)
lista_numeros[1] = 'No sé que está pasando'
print(lista_numeros)