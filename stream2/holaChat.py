# Input y Output
# nombre = input('¿Cuál es tu nombre? ')
# edad = input('¿Cuál es tu edad? ')
# print ('Hola soy ' + nombre + ' y tengo ' + str(edad) + ' años') # str = string (cadena de texto)
# print("soy :",nombre,"y tengo:",edad,"años.")

# String
# texto = 'muchas gracias por estar aquí, son los mejores'
# print(texto) # Imprime el valor de la variable texto
# print(texto.upper()) # Imprime la cadena en mayúsculas
# print(texto.lower()) # Imprime la cadena en minúsculas
# print(texto.capitalize()) # Imprime la cadena con la primer letra mayúscula
# print(texto.count('a')) # Cuenta la cantidad de letras 'a' en nuestra cadena


# Hola, mi nombre es: {Apellido}, {Nombre}, tengo {años} y mi comida favorita es/son: {comida}.

apellido = input('Apellido: ')
nombre = input('Nombre: ') 
años = input('Años: ') 
comida = input('Comida: ')

# print('Hola, mi nombre es: ',str(apellido),str(nombre),' tengo ',str(años),' y mi comida favorita es/son: ',str(comida))
print('Hola, mi nombre es:',apellido,nombre,'tengo',años,'y mi comida favorita es/son:',comida)
print('Hola, mi nombre es ' + apellido + ', ' + nombre + ' tengo ' + años + ' y mi comida favorita son los ' + comida + '.')
print('Hola, mi nombre es {}, {} tengo {} y mi comida favorita son los {}'.format(apellido, nombre, años, comida))
print('Hola, mi nombre es {0}, {1} tengo {2} y mi comida favorita son los {3}'.format(apellido, nombre, años, comida))
print('Hola, mi nombre es {1} {0} tengo {2} y mi comida favorita son los {3}'.format(apellido, nombre, años, comida))
# Solo funciona en Python 3
print(f'Hola, mi nombre es {apellido}, {nombre} tengo {años} y mi comida favoria son los {comida}')

